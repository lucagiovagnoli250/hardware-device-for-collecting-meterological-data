#include "buffer.h"
#include <pthread.h>
#include <stdlib.h>

struct _buffer{
    Type * data;
    int size;
    int head;
    int tail;
    int number;
    pthread_mutex_t lock;
};

Buffer * BufferInit(int size){
    Buffer * b = malloc(sizeof(Buffer));
    b->data = malloc (size*sizeof(Type));
    b->size = size;
    b->head = b->tail = b->number = 0;
    pthread_mutex_init(&(b->lock), NULL);
    return b;
}
void BufferDestroy (Buffer * b){
    free(b->data);
    pthread_mutex_destroy(&(b->lock));
    free(b);
}
int BufferNum (Buffer * b){
    pthread_mutex_lock(&(b->lock));
    int n = b->number;
    pthread_mutex_unlock(&(b->lock));
    return n;

}
void BufferInsert (Buffer * b, Type elem){
    pthread_mutex_lock(&(b->lock));

    while(b->number >= b->size -1){
        pthread_mutex_unlock(&(b->lock));
        pthread_yield(NULL);
        pthread_mutex_lock(&(b->lock));
    }

    (b->data)[b->head] = elem;
    b->head = (b->head + 1) % b->size;
    b->number ++;

    pthread_mutex_unlock(&(b->lock));
}


Type BufferGet (Buffer * b){
    pthread_mutex_lock(&(b->lock));

    while(b->number == 0){
        pthread_mutex_unlock(&(b->lock));
        pthread_yield(NULL);
        pthread_mutex_lock(&(b->lock));
    }

    Type ret = (b->data)[b->tail];
    b->tail = (b->tail + 1) % b->size;
    b->number--;

    pthread_mutex_unlock(&(b->lock));

    return ret;
}



