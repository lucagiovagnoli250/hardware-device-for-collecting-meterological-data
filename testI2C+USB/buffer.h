#ifndef BUFFER_H
#define BUFFER_H

/*
CODA CIRCOLARE IMPLEMENTATA COME ADT DI I CATEGORIA
THREAD SAFE, BLOCCANTE, PER VALORE
*/

typedef struct _buffer Buffer;

struct Dato{
	short dato1;
	short dato2;
	short dato3;
};

typedef struct Dato Type;  //SOSTITUIRE QUA IL TIPO DELLA CODA

/* in caso di code di più tipi, è consigliato usare una UNION

union Tipi{
    char carattere;
    int intero;
}
typedef union Tipi Type; */

Buffer * BufferInit(int size);
void BufferDestroy (Buffer * b);
int BufferNum (Buffer * b);
void BufferInsert (Buffer * b, Type elem);
Type BufferGet (Buffer * b);

#endif

//NOTA: Siccome nella nostra applicazione non è strettamente necessario che la coda sia bloccante, l'implementazione del blocco non è ottimizzata (fatta con una yield)
