PROCEDURA DA FARE PER SETTARE IL SISTEMA (SOLO LA PRIMA VOLTA)

1 - Scaricare l'ultima immagine di raspbian e metterla su la scheda SD
2 - Installare i seguenti pacchetti:

	sudo apt-get update

	sudo apt-get install libcurl4-openssl-dev
	sudo apt-get install libjansson-dev
	sudo apt-get install libusb-1.0-0-dev
	sudo apt-get install i2c-tools
	sudo apt-get install libi2c-dev

3 - Fare questa procedura per caricare i moduli kernel per i2c
	
	http://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

4 - Reboot

5 - Provare a far girare gli eseguibili di test 
