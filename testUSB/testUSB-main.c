/*
 ============================================================================
 Name        : testUSB-main.c
 Author      : Luca Giovagnoli
 Version     :
 Copyright   : Your copyright notice
 Description : Programma principale sul raspberry in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "raspberry-usb.h"

#define N 50

myusb_handler maniglia;

unsigned char* vettore;

int main(void) {

    myusb_init(&maniglia,0x046d,0xc03e);
    int z = 0;
    vettore = (unsigned char*) malloc(6*sizeof(char));
    short int umid = 0;
    short int temp = 0;
	
    while(1){
		sleep(2);
    
        z++;
        if(lettura_usb(maniglia,vettore,6)==0){
            
            umid = ((vettore[5] & 0x3F) << 8) + vettore[4];
            temp = ((vettore[3] << 8) + vettore[2]) >> 2;
            
			printf("%d Polvere: %hd Temperatura: %hd Umidità: %hd \n",z, * ((short int *) vettore),temp,umid);
        }
        
	}

	myusb_exit(maniglia);

	return EXIT_SUCCESS;
}