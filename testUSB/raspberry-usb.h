/*
 * raspberry-usb.h
 *
 *  Created on: May 2, 2013
 *      Author: luca
 */

#include <libusb-1.0/libusb.h>


#ifndef RASPBERRY_USB_H_
#define RASPBERRY_USB_H_

#define SUCCESSO 0
#define FAILURE -1

typedef libusb_device_handle * myusb_handler;

int myusb_init(libusb_device_handle ** maniglia, int vendor_id, int product_id);
int myusb_exit(libusb_device_handle * maniglia);
int lettura_usb(libusb_device_handle * maniglia, unsigned char* vettore, int lunghezza);

#endif /* RASPBERRY_USB_H_ */
