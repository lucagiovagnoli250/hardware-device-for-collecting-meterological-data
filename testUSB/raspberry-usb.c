/*
 ============================================================================
 Name        : raspberry usb.c
 Author      : Luca Giovagnoli
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>
#include "raspberry-usb.h"

int myusb_init(libusb_device_handle ** maniglia, int vendor_id, int product_id){

	if(libusb_init(NULL) != 0) {
		printf("Errore di inizializzazione. \n");
		return FAILURE;
	}

	if((*maniglia = libusb_open_device_with_vid_pid(NULL,vendor_id,product_id)) == NULL){
		printf("Errore di apertura del dispositivo con vid = %x e pid = %x. \n",vendor_id,product_id);
		return FAILURE;
	}
	printf("Dispositivo aperto\n");

	if(libusb_detach_kernel_driver(*maniglia,0)!=0){
		printf("Errore di qualche genere. \n");
		return FAILURE;
	}
	printf("kernel driver detached successfully\n");

	if(libusb_claim_interface(*maniglia,0)!=0){
		printf("Impossibile reclamare l'interfaccia. \n");
		return FAILURE;
	}
	printf("Interfaccia reclamata con successo.\nInizializzazione device usb riuscita\n");

	return SUCCESSO;
}

int myusb_exit(libusb_device_handle * maniglia){

	int ritorno;

	if((ritorno=libusb_release_interface(maniglia,0))!=0){
		printf("Problema nel rilascio dell'interfaccia. Errore numero %d\n",ritorno);
		return FAILURE;
	}
	if((ritorno=libusb_attach_kernel_driver(maniglia,0))!=0){
		printf("PROBLEM attaching kernel driver. Errore numero %d\n",ritorno);
		return FAILURE;
	}
	libusb_close(maniglia);
	libusb_exit(NULL);

	printf("Collegamento usb terminato con successo.\n");
	return SUCCESSO;

}

int lettura_usb(libusb_device_handle * maniglia, unsigned char* vettore, int lunghezza){

	int ritorno,letti,endpoint_addr;
	struct libusb_device_descriptor descrittore;
	struct libusb_config_descriptor * configurazione;
	libusb_device * dispositivo;

	dispositivo = libusb_get_device(maniglia);

	if((ritorno=libusb_get_device_descriptor(dispositivo,&descrittore))!=0){
		printf("Problema nell'ottenere il descrittore. Errore numero %d\n",ritorno);
		return FAILURE;
	}
	if((ritorno=libusb_get_config_descriptor(dispositivo,0,&configurazione))!=0){
		printf("Problema nell'ottenere la configurazione. Errore numero %d\n",ritorno);
		return FAILURE;
	}

	endpoint_addr=configurazione->interface->altsetting->endpoint->bEndpointAddress;

	if((ritorno=libusb_interrupt_transfer(maniglia,endpoint_addr,vettore,lunghezza,&letti,0))!=0){
		printf("Problema nel trasferimento dati. Errore numero %d\n",ritorno);
		return FAILURE;
	}
	//		printf("byte trasferiti : %d",letti);

	//Free a configuration descriptor obtained from libusb_get_config_descriptor().
	libusb_free_config_descriptor(configurazione);
	 
	return SUCCESSO;
}






