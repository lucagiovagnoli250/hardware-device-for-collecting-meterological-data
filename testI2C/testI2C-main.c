/*
 ============================================================================
 Name        : testI2C-main.c
 Author      : Francesco Santoro
 Version     :
 Copyright   : Your copyright notice
 Description : Programma principale sul raspberry in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "raspberry-i2c.h"


#define N 50
deviceI2C dev;

int main(void) {
    
	dev.address = 0x27;
	openDeviceI2C(&dev);
    measuresI2C m;
	
    while(1){
        sleep(2);
        
        readI2CMeasures(&dev, &m);
        
        printf("Temp: %d Umid: %d\n",m.temp,m.umid);
    }

	return EXIT_SUCCESS;
}