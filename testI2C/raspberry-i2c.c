//
//  raspberry-i2c.c
//  
//
//  Created by Francesco Santoro on 13/05/13.
//
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include "raspberry-i2c.h"

void delayMicrosecondsHard (unsigned int howLong)
{
    struct timeval tNow, tLong, tEnd;
    
    gettimeofday (&tNow, NULL);
    tLong.tv_sec  = howLong / 1000000;
    tLong.tv_usec = howLong % 1000000;
    timeradd (&tNow, &tLong, &tEnd);
    
    while (timercmp (&tNow, &tEnd, <))
        gettimeofday (&tNow, NULL) ;
}

void delayMicroseconds (unsigned int howLong)
{
    struct timespec sleeper;
    
    if (howLong == 0) return;
    else if (howLong < 100)
        delayMicrosecondsHard (howLong);
    else
    {
        sleeper.tv_sec  = 0 ;
        sleeper.tv_nsec = (long)(howLong * 1000);
        nanosleep (&sleeper, NULL) ;
    }
}

int openDeviceI2C(deviceI2C* device){
    
    char filename[20];
    int addr = device->address;
    int fd;
    
    sprintf(filename,"/dev/i2c-1",0);
    if ((fd = open(filename,O_RDWR)) < 0){
        printf("Error opening device\n");
        return -1;
    }
    
    if (ioctl(fd,I2C_SLAVE,addr) < 0){
        printf("ioctl failure\n");
        return -2;
    }
    
    device->fileDescriptor = fd;
    return 0;
    
}

int readI2CMeasures(deviceI2C* device, measuresI2C* results){
    
    char measureRequest = device->address << 1;
    int bytesRead;
    int fd = device->fileDescriptor;
    char tmp[4] = {0};
    
    short unsigned int umid = 0;
    short unsigned int temp = 0;
    
    if (write(fd,&measureRequest,1) < 0) {
        printf("Write failed\n");
        return -1;
    }
    
    delayMicroseconds(40000);
    
    if (bytesRead = read(fd,tmp,4) != 4) {
        printf("Read %d bytes\n",bytesRead);
        return bytesRead;
    }
    else {
        
        int status = tmp[0] >> 6;
        if (status > 1) {
            printf("Errore");
            return -2;
        }
        else {
            
            
            /*
            memcpy(&umid,tmp,2);
            memcpy(&temp,tmp+2,2);
            
            umid = umid & 0x3FFF;
            temp = temp >> 2;
            */
            
            umid = ((tmp[0] & 0x3F) << 8) + tmp[1];
            temp = ((tmp[2] << 8) + tmp[3]) >> 2;
            
            /*
            short int umidRead = ((tmp[0] & 0x3F) << 8) + tmp[1];
            short int tempRead = ((tmp[2] << 8) + tmp[3]) >> 2;
        
            /*
            int umid = umidRead / 164;
            int temp = tempRead * 0.01 - 40;
        
            printf("Status: %d  Temperature: %d  Umidity: %d\r",status,temp,umid);
            fflush(stdout);
            */
        
            results->umid = umid ;// / 164;
            results->temp = temp ;// * 0.01 - 40;
        
            return 4;
        }
    }
}

int closeDeviceI2C(deviceI2C* device){
    
    return close(device->fileDescriptor);

}
