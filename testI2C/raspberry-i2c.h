//
//  SensorI2Clib.h
//  
//
//  Created by Francesco Santoro on 13/05/13.
//
//

#ifndef _SensorI2Clib_h
#define _SensorI2Clib_h

typedef struct _deviceI2C{
    char address;
    int fileDescriptor;
} deviceI2C;

typedef struct _measures{
    short int umid;
    short int temp;
} measuresI2C;

void delayMicrosecondsHard (unsigned int howLong);
void delayMicroseconds (unsigned int howLong);
int openDeviceI2C(deviceI2C* device);
int readI2CMeasures(deviceI2C* device, measuresI2C* results);
int closeDeviceI2C(deviceI2C* device);

#endif
