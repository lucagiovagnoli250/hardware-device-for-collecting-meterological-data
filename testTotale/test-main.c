/*
 ============================================================================
 Name        : raspberry-main.c
 Author      : Luca Giovagnoli
 Version     :
 Copyright   : Your copyright notice
 Description : Programma principale sul raspberry in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include "../testI2C/raspberry-i2c.h"
#include "../testUSB/raspberry-usb.h"
#include "buffer.h"
#include "../testREMOTE/connection_auth.h"

#define N 50
#define SENSORNUMBER 5

myusb_handler maniglia;
Buffer * codaUsb;
Buffer * codaI2c;
deviceI2C dev;
char globalString[2000];

void* thread_usb_input(void* dati);
void* thread_I2C_input(void* dati);
void* thread_sendMail(void* h);


/*struct singlesensor {
	double mean, variance;
};

typedef struct invalues_ {
	struct singlesensor array[SENSORNUMBER];
} invalues;*/

struct timeval tv;

struct invalues calculateValues(struct Dato * UsbData, int nUsb, struct Dato * i2cData, int nI2c);

int main(void) {
	
	/* this is to prepare all the libcurl stuff - to be done one time only */
  	curl_global_init(CURL_GLOBAL_ALL);
    
    int flag_init, flag_send;
    FILE *f;

	pthread_t tid1,tid2, tid3;
	unsigned char* vettore;
    
	dev.address = 0x27;
	openDeviceI2C(&dev);

    myusb_init(&maniglia,0x0101,0x0101);
	
    codaUsb = BufferInit(10000);
	codaI2c = BufferInit(10000);

	pthread_create(&tid1,NULL,thread_usb_input,NULL);
	pthread_create(&tid2,NULL,thread_I2C_input,NULL);
	pthread_create(&tid3,NULL,thread_sendMail,NULL);
	
	flag_init = init_device();
    
    if (flag_init != 0){
    	printf("\nPROBLEMA REMOTE %d\n", flag_init);
    }
	
	while(1){
	
	gettimeofday(&tv, NULL);
	int timeToSleep = 0;
	if(tv.tv_sec % 300 > 240)
		{
		int i;
		int nUsb = BufferNum(codaUsb);
		struct Dato * arrayUsb = malloc(sizeof(struct Dato) * nUsb);
		for (i=0;i<nUsb;i++){
            		arrayUsb[i] = BufferGet(codaUsb);
		}
        
		int nI2c = BufferNum(codaI2c);
		struct Dato * arrayI2c = malloc(sizeof(struct Dato) * nI2c);
		for (i=0;i<nI2c;i++){
			arrayI2c[i] = BufferGet(codaI2c);
        }
        
		struct invalues to_send = calculateValues(arrayUsb, nUsb, arrayI2c, nI2c);
		
		free(arrayUsb);
		free(arrayI2c);
		
        printf("\n\nI2C temp %f (var %f), umid %f (var %f)\nUSB temp %f (var %f), umid %f (var %f), dust %f (var %f)\n\n",to_send.array[0].mean,to_send.array[0].variance, to_send.array[1].mean, to_send.array[1].variance,to_send.array[3].mean,to_send.array[3].variance, to_send.array[4].mean, to_send.array[4].variance, to_send.array[2].mean, to_send.array[2].variance);
		sprintf(globalString, "\n\nI2C temp %f (var %f), umid %f (var %f)\nUSB temp %f (var %f), umid %f (var %f), dust %f (var %f)\n\n",to_send.array[0].mean,to_send.array[0].variance, to_send.array[1].mean, to_send.array[1].variance,to_send.array[3].mean,to_send.array[3].variance, to_send.array[4].mean, to_send.array[4].variance, to_send.array[2].mean, to_send.array[2].variance);        
		
		f = fopen("message.txt", "w");
		fprintf(f, "%s", globalString);
		fclose(f);
		
		flag_send = send_data(to_send);
		timeToSleep = 60;
		}
		else
		{
			timeToSleep = 20;
		}
		
		sleep(timeToSleep);
	}

	myusb_exit(maniglia);
	curl_global_cleanup();
		
	return EXIT_SUCCESS;
}

void* thread_usb_input(void* dati){

	unsigned char* vettore;
	int z=0;
    short int dust = 0;
    short unsigned int temp = 0;
    short unsigned int umid = 0;
    
    vettore = (unsigned char*) malloc(6*sizeof(char));
	
    while(1){
		z++;
		
		if(lettura_usb(maniglia,vettore,6)==0){
			struct Dato d;
            
            memcpy(&dust,vettore,2);
           
            
            umid = ((vettore[5] & 0x3F) << 8) + vettore[4];
            temp = ((vettore[3] << 8) + vettore[2]) >> 2;
            
			//d.dato1 = dust ;
			d.dato1 =((dust * 500) / 740 );  //normalizzo ai ug/m3
			d.dato2 = temp * 0.01 - 40;
			d.dato3 = umid / 164;
            //printf("%d Polvere: %hd Temperatura: %hd Umidità: %hd %hd\n",z,d.dato1,d.dato2,d.dato3);
			BufferInsert(codaUsb, d);
        }
	}
}

void* thread_I2C_input(void* dati){
    
    int z = 0;
    while (1) {
        z++;
        measuresI2C m;
        readI2CMeasures(&dev, &m);
        struct Dato d;
        d.dato1 = 0;
        d.dato2 = m.temp * 0.01 -40 ;
        d.dato3 = m.umid / 164 ;
        //printf("%d Temp: %hd Umid: %hd\n",z,d.dato2,d.dato3);
        BufferInsert(codaI2c, d);
        
    }
	
}


struct invalues calculateValues(struct Dato * UsbData, int nUsb, struct Dato * i2cData, int nI2c){
	struct invalues ret;
	long int sum1=0, sum2=0, sum3=0;
	long int qsum1=0, qsum2=0, qsum3=0;
	int i;

	for (i = 0; i< nUsb; i++){
		sum1+=UsbData[i].dato1;
		sum2+=UsbData[i].dato2;
		sum3+=UsbData[i].dato3;
		
		qsum1+= UsbData[i].dato1 * UsbData[i].dato1;
		qsum2+= UsbData[i].dato2 * UsbData[i].dato2;
		qsum3+= UsbData[i].dato3 * UsbData[i].dato3;
	}
	
	ret.array[2].mean = ((double) sum1) / ((double) nUsb ) / 1000;  //usb dust normalizzo a mg/m3, divido media x mille e varianza x un milione
	ret.array[2].variance = (((double) qsum1) / ((double) nUsb ) - ret.array[2].mean * ret.array[2].mean)/1000000;

	ret.array[3].mean = ((double) sum2) / ((double) nUsb );  //usb temp
	ret.array[3].variance = ((double) qsum2) / ((double) nUsb ) - ret.array[3].mean * ret.array[3].mean;

	ret.array[4].mean = ((double) sum3) / ((double) nUsb );  //usb umid
	ret.array[4].variance = ((double) qsum3) / ((double) nUsb ) - ret.array[4].mean * ret.array[4].mean;


    sum2 = 0;
    sum3 = 0;
    qsum2 = 0;
    qsum3 = 0;
	for (i = 0; i< nI2c; i++){
		sum2+=i2cData[i].dato2;
		sum3+=i2cData[i].dato3;

		qsum2+= i2cData[i].dato2 * i2cData[i].dato2;
		qsum3+= i2cData[i].dato3 * i2cData[i].dato3;
	}

	ret.array[0].mean = ((double) sum2) / ((double) nI2c );   //rasp temp
	ret.array[0].variance = ((double) qsum2) / ((double) nI2c ) - ret.array[0].mean * ret.array[0].mean;
	ret.array[1].mean = ((double) sum3) / ((double) nI2c );   //rasp umid
	ret.array[1].variance = ((double) qsum3) / ((double) nI2c ) - ret.array[1].mean * ret.array[1].mean;

	return ret;
}

void* thread_sendMail(void* h){
	
	while(1)
	{
		
	system("curl -n --ssl-reqd --mail-from \"progsist14@gmail.com\" --mail-rcpt \"s198625@studenti.polito.it\" --url smtps://smtp.gmail.com:465 -T message.txt -u \"progsist14@gmail.com:provaprova\"");

	sleep(86000);
	}
	
	return NULL;
}