/*
 *  connection_auth.c
 *
 *  Created by Lorenzo Sciandra
 *	Smart City Project - Programmazione di Sistema
 *
 *	2013-05-27
 *
 *	
 *
 */

/* all standard includes are in the .h file */

#include "connection_auth.h"

/* I just need some global array variables */

/* these 3 are const because i don't want them to change */
const int sensor_id[SENSORNUMBER] = {101,202,303,404,505};
const char *sensor_unity_of_measure[SENSORNUMBER] = {"Celsius","%RH","ug/m^3","Celsius","%RH"};
const char *sensor_tags[SENSORNUMBER] = {"rasp_temp","rasp_humd","sens_dust","sens_temp","sens_humd"};

/*
 * 101 -> rasp temperature
 * 202 -> rasp humidity
 * 303 -> sensor dust
 * 404 -> sensor temperature
 * 505 -> sensor humidity
 */

int backup_files = 0;
/* this will keep count of the files (=SensorPost) not sended (because of some kind of error)
 * so then i will be able to re-read them the first time I can again send SensorPost.
 */

float LATITUDE = 10;
float LONGITUDE = 10;
float ACCURACY = 10.0;
float HEIGHT = 250.0;
int URL_SIZE = 0;
char raspb_wifi_mac[20], PORT[5], IPADR_FORMAT[30], USER[12], PASS[12], AUTH[25];
/* these are needed as global variables since i read them from file */

/*--------------------------------------------------------------------------------------*/

static size_t WriteMemoryCallback (void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;
    
    mem->memory = realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
    
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    
    return realsize;
}

char* get_time(char *time_out){
	time_t rawtime;
	struct tm * timeinfo;
  	char buffer [80];
    
  	time (&rawtime);
  	timeinfo = localtime (&rawtime);
  	/* ref: "send_timestamp":"2013-05-26T23:04:42.000+0200"*/
  	strftime (buffer,80,"%FT%T.000%z",timeinfo);
  	
  	/* ANSI C can't read ms, i add them by hand: since we work over 5 min intervals, no big deal */
  	
  	snprintf(time_out, 80,"%s", buffer);
	
	return(time_out);
}

json_t *pack_creator(struct invalues rawdata){
	
	/* since it has a complex structure, I create the parts and append them into coded */
	
	char time_now[80];		/* this will be just a variable i'll use to store temporary */
	int i;
	
	/* first thing I've to create is the sensor part, 'cause it's a mess */
	
	json_t *sensorarray = json_array();
    
	for (i = 0; i < SENSORNUMBER; i++){
		json_t *singleobj = json_pack ("{s:s, s:f, s:i, s:f, s:s}",
                                       "value_timestamp", get_time(time_now),
                                       "average_value",rawdata.array[i].mean ,
                                       "local_feed_id", sensor_id[i],
                                       "variance", rawdata.array[i].variance,
                                       "units_of_measurement", sensor_unity_of_measure[i]
                                       );
        
    	int flag = json_array_append_new(sensorarray, singleobj);
    	
    	if (flag == -1) {
			fprintf(stderr, "error: couldn't add the element to the array!\n");
			return NULL;
    	}
	}
	
	/* then the easy part, the position and the "head" */
	
	char timeMs[80];
	sprintf(timeMs, "%d", (int)time(NULL));
	
	json_t *sensor_position = json_pack ("{s:s, s:s, s:f, s:f, s:f, s:f}",
                                         "kind", "latitude#location",
                                         "timestampMs", timeMs,
                                         "latitude", LATITUDE,
                                         "longitude", LONGITUDE,
                                         "accuracy", ACCURACY,			/* actually for these two i don't know what to put */
                                         "height_meters", HEIGHT						
                                         );
    
	json_t *sensor_head = json_pack ("{s:s, s:s, s:o, s:o}",
                                     "raspb_wifi_mac", raspb_wifi_mac,
                                     "send_timestamp", get_time(time_now),
                                     "position", sensor_position,
                                     "sensor_values", sensorarray
                                     );
    
	return(sensor_head);
}

int register_device (void) {
	CURL *curl_post_device;
  	CURLcode status_post_device;
  	char *msg_device;								/* here i put my raw JSON to be send */
  	char url_device[URL_SIZE];						/* i'll use this to create the url */
  	
  	struct MemoryStruct chunk_post_device;
    
  	/* first i create it using jansson */
	  	
	json_t *device = json_pack ("{s:s,s:s}", "username", USER , "raspb_wifi_mac", raspb_wifi_mac);
    
    /* Returns the JSON representation of coded as a string */
    msg_device = json_dumps(device, JSON_PRESERVE_ORDER);
    
    //printf("\n\nSTO SCRIVENDO IO - PROVA MSG DEVICE JSON: %s\n\n", msg_device);
    
    json_decref(device);					/* and now i can "lose" the json object */
    
  	/*to make it work, i have to say in the header that it's JSON */
  	
  	struct curl_slist *headers_device = NULL;
    headers_device =  curl_slist_append(headers_device, "Content-Type: application/json");
    
    /* ok, now i can try to post */
    
    curl_post_device = curl_easy_init();
    
    chunk_post_device.memory = malloc(1);  	/* will be grown as needed by the realloc above */
  	chunk_post_device.size = 0;    			/* no data at this point */
    
    if (curl_post_device) {
    	
    	snprintf(url_device, URL_SIZE, IPADR_FORMAT, "SC/rest/apis/devices");
        
    	/* now i've to define the parameters */
  		curl_easy_setopt(curl_post_device, CURLOPT_URL, url_device);
  		
  		/* let's set to use HTTP Digest */
        //comment next line for testing use (test api does not need authentication)
        
  		curl_easy_setopt(curl_post_device, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        
   		/* set the usr and pwd */
        //comment next line for testing use (test api does not need authentication)
        
   		curl_easy_setopt(curl_post_device, CURLOPT_USERPWD, AUTH);
  		
  		/* and after that, the POST infos and datas */
  		curl_easy_setopt(curl_post_device, CURLOPT_HTTPHEADER, headers_device);
  		curl_easy_setopt(curl_post_device, CURLOPT_CUSTOMREQUEST,"POST");
  		curl_easy_setopt(curl_post_device, CURLOPT_POSTFIELDS, msg_device);
    	curl_easy_setopt(curl_post_device, CURLOPT_POSTFIELDSIZE, (long)strlen(msg_device));
    	
    	/* send all data to this function  */
  		curl_easy_setopt(curl_post_device, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        
  		/* we pass our 'chunk' struct to the callback function */
  		curl_easy_setopt(curl_post_device, CURLOPT_WRITEDATA, (void *)&chunk_post_device);
  		
  		/* some servers don't like requests that are made without a user-agent
         field, so we provide one */
  		curl_easy_setopt(curl_post_device, CURLOPT_USERAGENT, "gruppo14/1.3");
  		
  		/* Perform the request, status_auth will get the return code */
    	status_post_device = curl_easy_perform(curl_post_device);
   	 	
   		/* Check for errors */
    	if (status_post_device != CURLE_OK) {
      		fprintf(stderr, "error: unable to post data to %s:\n", url_device);
        	fprintf(stderr, "%s\n", curl_easy_strerror(status_post_device));
        	return -1;	
    	} else {
    		/* just to check that it worked, i print the "message" from the server */
    		printf("%lu bytes retrieved\n", (long)chunk_post_device.size);
    		printf("STO SCRIVENDO IO - RISPOSTA POST\n\n\n%s", (char*)chunk_post_device.memory);
    	}
	}
	
	/* free slist */
    curl_slist_free_all (headers_device);
    
    /* always cleanup */
    curl_easy_cleanup (curl_post_device);
    
    free(msg_device);
    
    if(chunk_post_device.memory) {
   		free(chunk_post_device.memory);
    }
    
	return 0;
}

int register_feeds(void) {
	CURL *curl_feed;
  	CURLcode status_feed;
  	char *feed;													/* here i put my raw JSON to be send */
  	char url_feed[URL_SIZE], url_tail[URL_SIZE];				/* i'll use this to create the different urls */
	int i;
    
	struct MemoryStruct chunk_feed;
      	
  	for (i = 0; i < SENSORNUMBER; i++){
        
		json_t *json_feed = json_pack ("{s:s, s:i, s:s}", 
										"tags", sensor_tags[i] ,
										"local_feed_id", sensor_id[i],
										"raspb_wifi_mac", raspb_wifi_mac
										);
        
        /* Returns the JSON representation of coded as a string */
    	feed = json_dumps(json_feed, JSON_PRESERVE_ORDER);							
        
    	//printf("\n\nSTO SCRIVENDO IO - PROVA FEED JSON: %s\n\n", feed);
        
        /* and now i can "lose" the json object */
    	json_decref(json_feed);									
  	  	
  		/*to make it work, i have to say in the header that it's JSON */
        
  		struct curl_slist *headers_feed = NULL;
    	headers_feed =  curl_slist_append(headers_feed, "Content-Type: application/json");
        
    	curl_feed = curl_easy_init();
        
    	chunk_feed.memory = malloc(1);  /* will be grown as needed by the realloc above */
  		chunk_feed.size = 0;    		/* no data at this point */
        
    	if (curl_feed) {
    		
    		snprintf(url_tail, URL_SIZE, "SC/rest/apis/devices/%s/feeds", raspb_wifi_mac);
			snprintf(url_feed, URL_SIZE, IPADR_FORMAT, url_tail);
            
   			printf("\n\nSTO SCRIVENDO IO - PROVA URL: %s\n\n", url_feed);
   			
    		/* now i've to define the parameters */
  			curl_easy_setopt(curl_feed, CURLOPT_URL, url_feed);
  			
  			/* let's set to use HTTP Digest */
            //comment next line for testing use (test api does not need authentication)
            
  			curl_easy_setopt(curl_feed, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        
   			/* set the usr and pwd */
            //comment next line for testing use (test api does not need authentication)
            
   			curl_easy_setopt(curl_feed, CURLOPT_USERPWD, AUTH);
  			
  			/* and now POST data and infos */
  			curl_easy_setopt(curl_feed, CURLOPT_HTTPHEADER, headers_feed);
  		  	curl_easy_setopt(curl_feed, CURLOPT_CUSTOMREQUEST,"POST");
    		curl_easy_setopt(curl_feed, CURLOPT_POSTFIELDS, feed);
    		curl_easy_setopt(curl_feed, CURLOPT_POSTFIELDSIZE, (long)strlen(feed));
            
    		/* send all data to this function  */
  			curl_easy_setopt(curl_feed, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
            
  			/* we pass our 'chunk' struct to the callback function */
  			curl_easy_setopt(curl_feed, CURLOPT_WRITEDATA, (void *)&chunk_feed);
            
  			/* some servers don't like requests that are made without a user-agent
             field, so we provide one */
  			curl_easy_setopt(curl_feed, CURLOPT_USERAGENT, "gruppo14/1.3");
            
	  		/* Perform the request, status_auth will get the return code */
    		status_feed = curl_easy_perform(curl_feed);
            
   			/* Check for errors */
    		if(status_feed != CURLE_OK){
      			fprintf(stderr, "error: unable to post data to %s:\n", url_feed);
        		fprintf(stderr, "%s\n", curl_easy_strerror(status_feed));
        		return -1;
    		} else {
                
    			/* just to check that it worked, i print the "message" from the server.
                 this one put the "raw" json text into a made-up variable and extract the
                 unique_feed_id */
				
    			printf("%lu bytes retrieved\n", (long)chunk_feed.size);
    			printf("STO SCRIVENDO IO - RISPOSTA FEED\n\n%s", (char *)chunk_feed.memory);
    		}
		}
        
		/* free slist */
    	curl_slist_free_all (headers_feed);
        
    	/* always cleanup */
    	curl_easy_cleanup(curl_feed);
        
   		free(feed);
        
    	if(chunk_feed.memory) {
   			free(chunk_feed.memory);
   		}
    }
	
	return 0;
}

int read_file(void){
	FILE *info;
	int url_size;
	float latitude, longitude;
	char mac[20], port[5], ipadr_format[30], user[12], pass[12], auth[25];
	
	info = fopen("info.txt", "r");
	
	if (!info) {
		fprintf(stderr,"error: cannot open info file!");
		return -1;
	}
	
	while (!feof(info)) {
		fscanf(info, "%f", &latitude);
		fscanf(info, "%f", &longitude);
		fscanf(info, "%s", mac);
		fscanf(info, "%s", port);	
		fscanf(info, "%s", ipadr_format);	
		fscanf(info, "%d", &url_size);	
		fscanf(info, "%s", user);	
		fscanf(info, "%s", pass);	
		fscanf(info, "%s", auth);			
	}
	
	LATITUDE = latitude;
	LONGITUDE = longitude;
	strcpy(raspb_wifi_mac, mac);
	strcpy(PORT, port);
	strcpy(IPADR_FORMAT, ipadr_format);
	URL_SIZE = url_size;
	strcpy(USER, user);
	strcpy(PASS, pass);
	strcpy(AUTH, auth);
		
	fclose(info);
	
	return 0;
}

int test_auth(void){
	CURL *curl_auth_test;
	CURLcode status_auth_test;
  	char url_auth_test[URL_SIZE], url_tail_test[URL_SIZE];						/* i'll use this to create the url */
  	long code;
	
	struct MemoryStruct chunk_auth_test;
  	
  	/* first thing first, I create an handle for authentication */
  	curl_auth_test = curl_easy_init();
    
  	chunk_auth_test.memory = malloc(1);  /* will be grown as needed by the realloc above */
  	chunk_auth_test.size = 0;    /* no data at this point */
  	
  	if(curl_auth_test) {
  	
  		snprintf(url_tail_test, URL_SIZE, "SC/rest/apis/auth/%s", USER);
  		snprintf(url_auth_test, URL_SIZE, IPADR_FORMAT,  url_tail_test);
  		
  		printf("\n\nSTO SCRIVENDO IO - PROVA URL: %s\n\n", url_auth_test);
  		
  		/* set url */
  		curl_easy_setopt(curl_auth_test, CURLOPT_URL, url_auth_test);
  		
  		/* let's set to use HTTP Digest */
  		curl_easy_setopt(curl_auth_test, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        
   		/* set the usr and pwd */
   		curl_easy_setopt(curl_auth_test, CURLOPT_USERPWD, AUTH);
   		
   		/* send all data to this function  */
  		curl_easy_setopt(curl_auth_test, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
   		
   		/* we pass our 'chunk' struct to the callback function */
  		curl_easy_setopt(curl_auth_test, CURLOPT_WRITEDATA, (void *)&chunk_auth_test);
   		
   		/* the thing i want to try is the GET to verify if the auth works */
        curl_easy_setopt(curl_auth_test,CURLOPT_CUSTOMREQUEST,"GET");
    	
   		/* Perform the request, status_auth will get the return code */
    	status_auth_test = curl_easy_perform(curl_auth_test);
   	 	
   		/* Check for errors */
    	if(status_auth_test != CURLE_OK){
      		
      		fprintf(stderr, "error: unable to request data from %s:\n", url_auth_test);
        	fprintf(stderr, "%s\n", curl_easy_strerror(status_auth_test));
        	return -1;
        	
    	} else {
            
    		curl_easy_getinfo(curl_auth_test, CURLINFO_RESPONSE_CODE, &code);
    		if(code != 200) {
        		fprintf(stderr, "error: server responded with code %ld\n", code);
        		return -1;
    		}
    		
    		/* Ok, if I'm here the auth worked - in chunk.memory i've the message */
    		/* just to check that it worked, i print the "message" from the server */
    		
    		printf("%lu bytes retrieved\n", (long)chunk_auth_test.size);
    		printf("STO SCRIVENDO IO --- GET TEST AUTENTICAZIONE\n\n\n%s", (char*)chunk_auth_test.memory);
    	}
  	}
    
    /* cleanup curl stuff */
  	curl_easy_cleanup(curl_auth_test);
    
  	if(chunk_auth_test.memory) {
   		free(chunk_auth_test.memory);
	}
	
	return 0;
}

int write_backup (char *msg){
	FILE *backup;
	char name_backup[40];
	
	snprintf(name_backup, 40, "backup/backup_number_%d.txt", backup_files);
	
	printf("\n\nnome file di backup: %s\n\n", name_backup);
	
	backup = fopen(name_backup, "w");
	
	if (!backup) {
		fprintf(stderr,"error: cannot create backup file!");
		return -1;
	}
	
	fprintf(backup, "%s", msg);
	
	/* if i'm here it means that the writing procedure worked */
	
	backup_files++;
	
	fclose(backup);
	
	return 0;
}

int resend_backup (void){
	FILE *backup;
	int i, flag_send;
	char *msg_send; 						
	char name_backup[40];
	size_t nread;
	int chunk = 1024;
	/* read 1024 bytes at a time - this because usually a SensorPost is around 950 bytes */
	
	for (i = 0; i < backup_files; i++){
	
		msg_send = malloc(chunk);			
		
		if (msg_send == NULL){
			printf("ERROR: msg_send non allocated!");
		}
		
		snprintf(name_backup, 40, "backup/backup_number_%d.txt", i);
	
		printf("\n\nnome file di backup restored: %s\n\n", name_backup);
	
		backup = fopen(name_backup, "r");
	
		if (!backup) {
			fprintf(stderr,"error: cannot read %s!", name_backup);
			return -1;
		}
		
		while ((nread = fread(msg_send, 1, chunk, backup)) > 0) {
			printf("\n\nLa funzione fread ha restituito %d\n\n", (int)nread);		
		}
		
		printf("\n\nmessaggio letto nel file: %s\n\n", msg_send);
		
		flag_send = sender_post(msg_send);
    	    	
    	if (flag_send == -1) {
        	fprintf(stderr, "\nerror: can send!\n");
    		return -1;
    	}
    
		fclose(backup);
		free(msg_send);
	}
	
	backup_files = 0;
	
	return 0;
}

int sender_post(char *msg_send){
	CURL *curl_send;
  	CURLcode status_send;
  	char url_send[URL_SIZE], url_tail[URL_SIZE];				/* i'll use this to create the different urls */
    
    int flag_backup;
    
	struct MemoryStruct chunk_send;
    
  	/*to make it work, i have to say in the header that it's JSON */
  	
  	struct curl_slist *headers_send = NULL;
    headers_send =  curl_slist_append(headers_send, "Content-Type: application/json");
    
    curl_send = curl_easy_init();
    
    chunk_send.memory = malloc(1);  	/* will be grown as needed by the realloc above */
  	chunk_send.size = 0;    			/* no data at this point */
    
    if (curl_send) {
    	snprintf(url_tail, URL_SIZE, "SC/rest/apis/device/%s/posts", raspb_wifi_mac);
		snprintf(url_send, URL_SIZE, IPADR_FORMAT, url_tail);
   		printf("\n\nSTO SCRIVENDO IO - PROVA URL: %s\n\n", url_send);
    	
    	/* now i've to define the parameters */
  		curl_easy_setopt(curl_send, CURLOPT_URL, url_send);
  		
  		/* let's set to use HTTP Digest */
        //comment next line for testing use (test api does not need authentication)
        
  		curl_easy_setopt(curl_send, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        
   		/* now i set the usr and pwd */
        //comment next line for testing use (test api does not need authentication)
        
   		curl_easy_setopt(curl_send, CURLOPT_USERPWD, AUTH);
  		
  		curl_easy_setopt(curl_send, CURLOPT_HTTPHEADER, headers_send);
  		
  		curl_easy_setopt(curl_send, CURLOPT_CUSTOMREQUEST,"POST");
    	curl_easy_setopt(curl_send, CURLOPT_POSTFIELDS, msg_send);
    	curl_easy_setopt(curl_send, CURLOPT_POSTFIELDSIZE, (long)strlen(msg_send));
    	
    	/* send all data to this function  */
  		curl_easy_setopt(curl_send, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        
  		/* we pass our 'chunk' struct to the callback function */
  		curl_easy_setopt(curl_send, CURLOPT_WRITEDATA, (void *)&chunk_send);
  		
  		/* some servers don't like requests that are made without a user-agent
         field, so we provide one */
  		curl_easy_setopt(curl_send, CURLOPT_USERAGENT, "gruppo14/1.3");	
  		
  		/* Perform the request, status_auth will get the return code */
    	status_send = curl_easy_perform(curl_send);
   	 	
   		/* Check for errors */
    	if(status_send != CURLE_OK){
      		fprintf(stderr, "error: unable to post data to %s:\n", url_send);
        	fprintf(stderr, "%s\n", curl_easy_strerror(status_send));
        	flag_backup = write_backup(msg_send);
        	return -1;
    	} else {
            
    		/* just to check that it worked, i print the "message" from the server */
    		printf("%lu bytes retrieved\n", (long)chunk_send.size);
    		printf("STO SCRIVENDO IO - RISPOSTA SENSORPOST\n\n\n%s", (char*)chunk_send.memory);
    		
    	}
		
	}
	
	/* free slist */
    curl_slist_free_all (headers_send);
    
    /* always cleanup */
    curl_easy_cleanup(curl_send);
        
    if(chunk_send.memory){
   		free(chunk_send.memory);
    }
    
    return 0;
}

int init_device (void) {
  	int flag_reg, flag_feed, flag_read;
	int counter_reg = 0;
	int counter_feed = 0;
	
	/* first thing needed is to read from file the LONG, LAT and raspb_wifi_mac */
	
	flag_read = read_file();
	
	if (flag_read == -1){
		return 101;
	}
	
	/* device registration */
	
	do {
	
		flag_reg = register_device();
	
		if (flag_reg == -1) {
			counter_reg++;
			sleep (10);
			/* first type of "security": since it's about connecting to a server,
			   maybe it just a temporary condition which doesn't allow registration
			   so I wait 1 sec between a try and another */
		}
	
	} while (flag_reg == -1 && counter_reg < 10);
	
	if (counter_reg == 10) {
		/* if I'm here it means we have big problems - I return a code 101 (chosen by me) 
		   which identify this error so i know it happened here */
		return 102;
	}
	
	/* feed registrations */
	
	do {
	
		flag_feed = register_feeds();	
		
		if (flag_feed == -1) {
			counter_feed++;
			sleep (10);
			/* first type of "security": since it's about connecting to a server,
			   maybe it just a temporary condition which doesn't allow registration
			   so I wait 1 sec between a try and another */
		}
	
	} while (flag_feed == -1 && counter_feed < 10);
	
	if (counter_feed == 10) {
		/* if I'm here it means we have big problems - I return a code 102 (chosen by me) 
		   which identify this error so i know it happened here */
		return 103;
	}
	
	return 0;
}

int send_data(struct invalues rawdata) {
  	char *msg_send;												/* here i put my raw JSON to be send */
    
    int flag_send, flag_resend;
  	
  	printf("\nTime to play JSON - the sensor post part\n");
  		
	/* for the "example" purpose, i create right here a fake struct similar to the final one */
	
	json_t *coded_final = pack_creator(rawdata);
    
    msg_send = json_dumps(coded_final, JSON_PRESERVE_ORDER);							// Returns the JSON representation of coded as a string
    
    //printf("\n\nSTO SCRIVENDO IO - PROVA MSG JSON: %s\n\n", msg_send);
    
    json_decref(coded_final);									// and now i can "lose" the json object
    
  	flag_send = sender_post(msg_send);
    
    if (flag_send == -1) {
        fprintf(stderr, "\nerror: can send!\n");
    	return -1;
    } else {
    	if (backup_files > 0) {
    		flag_resend = resend_backup();
    	}
    }
    
    free(msg_send);

    return 0;
}