/*
 *  connection_auth.h
 *
 *  Created by Lorenzo Sciandra
 *	Smart City Project - Programmazione di Sistema
 *
 *	2013-05-27
 *	v0.1
 */

#ifndef CONNECTION_AUTH_H
#define CONNECTION_AUTH_H

/* first the includes */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <curl/curl.h>
#include <jansson.h>

/* here i put all the defines I need in the code */

#define SENSORNUMBER 5

/* then the struct */

struct MemoryStruct {			/* this one is needed for cURL, as the function WriteMemoryCallback */
    char *memory;
    size_t size;
};

/* these two are here just to test */

struct singlesensor {
	double mean, variance;
};

struct invalues {
	struct singlesensor array[SENSORNUMBER];
};

/* and then the functions */

/* first the "utilities" */

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
char* get_time(char *time_out);
json_t *pack_creator(struct invalues rawdata);
int register_device (void);
int register_feeds (void);

int test_auth (void);
/* this one is just to test the HTTP DIGEST */


int read_file (void);
/* with this one i read the info from a file (info.txt) */


int write_backup (char *msg);
/*
 * to this one I pass a SensorPost which cannot be send in a specific moment for
 * connection issues. It writes it down on a newly generated .txt in the folder "backup" 
 * The name of the file has a specific format
 * 								"backup_file_(number).txt"
 * where number is incremented: every SensorPost gets its own txt (for readability issues)
 */


int resend_backup (void);
/*
 * this function is called into send_data, when the send has gone well, and there are
 * sensor_post backup to be send.
 * It is simply a for-cycle which read all the backup files left to be send.
 */


int sender_post(char *msg_send);
/*
 * this is the generic function to POST SensorPost - it used both by "resend_backup"
 * and "send_data".
 */

/*--------------------------------------------------------------------------------------*/

/*
 * then the ones we'll use in the rasp_main - all of them return 0
 * when everything is good, and specific codes if not
 */


int init_device(void);
/*
 * Three things happens:
 *
 * 1) the function reads from the file "info.txt" the info necessary to work
 *		associated error code: 101	
 *
 * 2) the device, with his MAC address, is registered with username "gruppo14"
 *		associated error code: 102	
 *
 * 3) all the 5 feeds ( = # sensors) are registered
 *		associated error code: 103	
 *
 */



int send_data(struct invalues rawdata);
/*
 * this function here is the one called every 5 minutes and needs as input
 * the new data for the 5 sensors. It create the JSON data to be send to the server
 * and then send them in the auth mode using "sender_post".
 * if something goes wrong, it save in a file the JSON message to be send using "write_backup"
 * - then, as soon as it can, it will resend them using "resend_backup".
 */

#endif