/*
 *  cURLmain.c
 *  
 *  Created by Lorenzo Sciandra
 *	Smart City Project - Programmazione di Sistema
 *
 *	2013-05-27
 *
 * 	This is just to test the connection_auth.h/.c
 * 	To make everything works, after installing curl and jansson
 *	All you need to write in the terminal is
 *	"gcc cURLmain_auth.c connection_auth.h connection_auth.c -o cURLmain_auth.out -l curl -l jansson"
 *	and, to test:
 *	"./cURLmain_auth.out"
 */
 
#include "connection_auth.h"

/* this is just for testing the connection .h and .c */

int main(void) {
	/* this is to prepare all the libcurl stuff - to be done one time only */
  	curl_global_init(CURL_GLOBAL_ALL);
    
    int i, flag_init, flag_send;
    
    flag_init = init_device();
    
    if (flag_init != 0){
    	printf("\nPROBLEMA %d\n", flag_init);
    }
        
	struct invalues rawdata;
	for (i = 0; i < SENSORNUMBER; i++){
		rawdata.array[i].mean = (double)i;
		rawdata.array[i].variance = 2.0 * (double)i;
	}
	
    flag_send = send_data(rawdata);
    
    printf("\n\n ORA ASPETTO 20 SECONDI! \n \n");
    sleep (20);
    printf("\n\nRIPARTO!\n\n");
    
    for (i = 0; i < SENSORNUMBER; i++){
		rawdata.array[i].mean = 4.0 + (double)i;
		rawdata.array[i].variance = 3.0 * (double)i;
	}
	
    flag_send = send_data(rawdata);
    
    printf("\n\n ORA ASPETTO 20 SECONDI! \n \n");
    sleep (20);
    printf("\n\nRIPARTO!\n\n");
    
    for (i = 0; i < SENSORNUMBER; i++){
		rawdata.array[i].mean = 10 + (double)i;
		rawdata.array[i].variance = 6.0 * (double)i;
	}
	
    flag_send = send_data(rawdata);
    
	curl_global_cleanup();
  	return 0;
}